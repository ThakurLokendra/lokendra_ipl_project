const fs = require('fs');
function bestEconomyInSuperOver(deliveriesData) {
    let bowlerOversAndRuns = {};
    let economy = {};

    for (let index = 0; index < deliveriesData.length; index++) {
        if (deliveriesData[index]['is_super_over'] == '1') {
            if (bowlerOversAndRuns[deliveriesData[index]['bowler']]) {
                if (deliveriesData[index]['ball'] == '1') {
                    bowlerOversAndRuns[deliveriesData[index]['bowler']].overs += 1;
                }
                bowlerOversAndRuns[deliveriesData[index]['bowler']].runs += Number.parseInt(deliveriesData[index]['total_runs']);
            } else {
                bowlerOversAndRuns[deliveriesData[index]['bowler']] = { overs: 1, runs: Number.parseInt(deliveriesData[index]['total_runs']) }
            }
        }
    }
    for (const key in bowlerOversAndRuns) {
        economy[key] = (bowlerOversAndRuns[key].runs / bowlerOversAndRuns[key].overs);
    }

    let bestEconomyNumber = Number.MAX_VALUE;
    let bowlerName;

    for (const key in economy) {
        if (economy[key] < bestEconomyNumber) {
            bestEconomyNumber = economy[key];
            bowlerName = key;
        }
    }

    let bestEconomy = {};
    bestEconomy[bowlerName] = bestEconomyNumber;

    for (const key in economy) {
        if (economy[key] == bestEconomyNumber) {
            bestEconomy[key] = bestEconomyNumber;
        }
    }

    fs.writeFile('../public/output/bestEconomicalBowlerInSuperOver.json', JSON.stringify(bestEconomy, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })

}



module.exports = bestEconomyInSuperOver;