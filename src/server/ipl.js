const matchesPlayedPerYear = require("./matchesPlayedPerYear.js");
const matchesWonPerTeam = require("./matchesWonPerTeam.js");
const extraRunsConcededIn2016 = require("./extraRunsConcededIn2016.js");
const topEconomicalBowler2015 = require("./topEconomicalBowler2015");
const bestEconomyInSuperOver = require("./bestEconomyBowlerInsuperOvers.js");
const strikeRatePerSeason = require('./strikeRateOfAPlayerPerSeason.js');
const WonMatchAndToss = require('./wonMatchAndToss.js');
const mostPlayerOfMatchAwardsPerSeason = require("./mostPlayerOfTheMatchAward.js");
const highestTimePlayerDismissed = require("./highestTimePlayerDismissed");

module.exports = {
    matchesPlayedPerYear,
    matchesWonPerTeam,
    extraRunsConcededIn2016,
    topEconomicalBowler2015,
    bestEconomyInSuperOver,
    strikeRatePerSeason,
    WonMatchAndToss,
    mostPlayerOfMatchAwardsPerSeason,
    highestTimePlayerDismissed
}

