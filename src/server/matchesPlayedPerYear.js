const fs = require('fs');

function matchesPlayedPerYear(matchesData) {
    let mathcesPlayed = {};

    for (let index = 0; index < matchesData.length; index++) {
        if (mathcesPlayed[matchesData[index]['season']]) {
            mathcesPlayed[matchesData[index]['season']] += 1;
        } else {
            mathcesPlayed[matchesData[index]['season']] = 1;
        }
    }

    fs.writeFile('../public/output/matchesPlayedPerYear.json', JSON.stringify(mathcesPlayed, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })

}



module.exports = matchesPlayedPerYear;