const fs = require('fs');

function highestTimePlayerDismissed(deliveriesData) {
    let numberOfTimesPlayerDismissed = {};

    for (let index = 0; index < deliveriesData.length; index++) {
        if (deliveriesData[index]['player_dismissed'] && deliveriesData[index]["dismissal_kind"] != "retired hurt" && deliveriesData[index]["dismissal_kind"] != "obstructing the field") {
            if (numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']]) {
                if (deliveriesData[index]['feilder']) {
                    if (numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['fielder']]) {
                        numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['fielder']] += 1;
                    } else {
                        numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['fielder']] = 1;
                    }

                } else {

                    if (numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['bowler']]) {
                        numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['bowler']] += 1;
                    } else {
                        numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['bowler']] = 1;
                    }

                }
            } else {
                numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']] = {};
                if (deliveriesData[index]['fielder']) {
                    numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['fielder']] = 1;
                } else {
                    numberOfTimesPlayerDismissed[deliveriesData[index]['player_dismissed']][deliveriesData[index]['bowler']] = 1;
                }
            }
        }
    }

    let max = Number.MIN_VALUE;
    for (const key in numberOfTimesPlayerDismissed) {
        let array = Object.entries(numberOfTimesPlayerDismissed[key]);
        for (let index = 0; index < array.length; index++) {
            if (array[index][1] > max) {
                max = array[index][1];
            }
        }
    }

    let dismissedByAndTimes = [];
    for (const key in numberOfTimesPlayerDismissed) {
        let array = Object.entries(numberOfTimesPlayerDismissed[key]);

        for (let index = 0; index < array.length; index++) {
            if (array[index][1] === max) {
                dismissedByAndTimes.push([key,...array[index]]);
            }
        }
    }

    let mostNumberOfTimesPlayerDismissed = {};
    for (const key in dismissedByAndTimes) {
        if (mostNumberOfTimesPlayerDismissed[dismissedByAndTimes[key][0]]) {
            mostNumberOfTimesPlayerDismissed[dismissedByAndTimes[key][0]][dismissedByAndTimes[key][1]] = dismissedByAndTimes[key][2];
        } else {
            mostNumberOfTimesPlayerDismissed[dismissedByAndTimes[key][0]] = {};
            mostNumberOfTimesPlayerDismissed[dismissedByAndTimes[key][0]][dismissedByAndTimes[key][1]] = dismissedByAndTimes[key][2];
        }
    }

console.log(mostNumberOfTimesPlayerDismissed);

    fs.writeFile('../public/output/highestTimePlayerDismissed.json', JSON.stringify(mostNumberOfTimesPlayerDismissed, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })

}

module.exports = highestTimePlayerDismissed;