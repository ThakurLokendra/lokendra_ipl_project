const fs = require('fs');
function strikeRatePerSeason(matchesData, deliveriesData) {
    let id = [];
    let strikeRate = {};

    for (let index = 0; index < matchesData.length; index++) {
        id.push([matchesData[index]['season'], matchesData[index]['id']]);
    }

    for (let outerIndex = 0; outerIndex < id.length; outerIndex++) {
        for (let innerIndex = 0; innerIndex < deliveriesData.length; innerIndex++) {
            if ((id[outerIndex][1] === deliveriesData[innerIndex]['match_id'])) {
                if (strikeRate[id[outerIndex][0]]) {
                    if (strikeRate[id[outerIndex][0]][deliveriesData[innerIndex]['batsman']] && deliveriesData[innerIndex]['batsman'] == 'V Kohli') {
                        if (deliveriesData[innerIndex]['wide_runs'] == '0') {
                            strikeRate[id[outerIndex][0]][deliveriesData[innerIndex]['batsman']].runs += Number.parseInt(deliveriesData[innerIndex]['total_runs']);
                            strikeRate[id[outerIndex][0]][deliveriesData[innerIndex]['batsman']].balls += 1;
                        }
                    } else {
                        if (deliveriesData[innerIndex]['wide_runs'] == '0' && deliveriesData[innerIndex]['batsman'] == 'V Kohli') {
                            strikeRate[id[outerIndex][0]][deliveriesData[innerIndex]['batsman']] = { runs: Number.parseInt(deliveriesData[innerIndex]['total_runs']), balls: 1 };
                        }

                    }

                } else {
                    strikeRate[id[outerIndex][0]] = {};
                    if (deliveriesData[innerIndex]['wide_runs'] == '0' && deliveriesData[innerIndex]['batsman'] == 'V Kohli') {
                        strikeRate[id[outerIndex][0]][deliveriesData[innerIndex]['batsman']] = { runs: Number.parseInt(deliveriesData[innerIndex]['total_runs']), balls: 1 };
                    }

                }
            }
        }

    }


    for (const key in strikeRate) {
        strikeRate[key]['V Kohli'].strikeRate = (strikeRate[key]['V Kohli'].runs / strikeRate[key]['V Kohli'].balls).toFixed(2);
    }


    let strikeRateFinal = {};
    strikeRateFinal['V kohli']= {};
    for (const key in strikeRate) {
        let strikeRateArr = Object.entries(strikeRate[key]);

        for (let index = 0; index < strikeRateArr.length; index++) {
            strikeRateFinal['V kohli'][key] = strikeRateArr[index][1].strikeRate;
        }


    }



    
    fs.writeFile('../public/output/strikeRatePerSeason.json', JSON.stringify(strikeRateFinal, null, 4), 'utf-8', (error) => {
        if (error) {
            console.log(error);
        }
    });



}

module.exports = strikeRatePerSeason;