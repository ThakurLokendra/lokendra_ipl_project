const fs = require('fs');

function mostPlayerOfMatchAwardsPerSeason(matches) {

    const playerOfMatchAwardsPerYear = {};

    for (let index = 0; index < matches.length; index++) {

        if (!playerOfMatchAwardsPerYear[matches[index]["season"]]) {
            playerOfMatchAwardsPerYear[matches[index]["season"]] = {};
            playerOfMatchAwardsPerYear[matches[index]["season"]][matches[index]["player_of_match"]] = 1;
        }

        else if (!playerOfMatchAwardsPerYear[matches[index]["season"]][matches[index]["player_of_match"]]) {
            playerOfMatchAwardsPerYear[matches[index]["season"]][matches[index]["player_of_match"]] = 1;
        }

        else {
            playerOfMatchAwardsPerYear[matches[index]["season"]][matches[index]["player_of_match"]] += 1;
        }
    }

    const mostPlayerOfMatchAwardsPerYear = {};
    for (const key in playerOfMatchAwardsPerYear) {
        let array = Object.entries(playerOfMatchAwardsPerYear[key]);
        mostPlayerOfMatchAwardsPerYear[key] = {};
        let maxAward = Number.MIN_VALUE;
        for (let index = 0; index < array.length; index++) {
            if (array[index][1] > maxAward) {
                maxAward = array[index][1];
            }
        }

        for (let index = 0; index < array.length; index++) {
            if (array[index][1] == maxAward) {
                mostPlayerOfMatchAwardsPerYear[key][array[index][0]] = maxAward;
            }
        }
    }


    fs.writeFile('../public/output/mostPlayerOfMatchAwardsPerSeason.json', JSON.stringify(mostPlayerOfMatchAwardsPerYear, null, 4), 'utf-8', (error) => {
        if (error) {
            console.log(error);
        }
    });

}


module.exports = mostPlayerOfMatchAwardsPerSeason;