const matches = '/home/lokendra/mountblue/ipl_project/src/data/matches.csv';
const deliveries = '/home/lokendra/mountblue/ipl_project/src/data/deliveries.csv';
const csv = require('csvtojson');
const { matchesWonPerTeam } = require('./ipl.js');
const { extraRunsConcededIn2016 } = require('./ipl.js');
const { matchesPlayedPerYear } = require('./ipl.js');
const { topEconomicalBowler2015 } = require('./ipl.js');
const { bestEconomyInSuperOver } = require('./ipl.js');
const { strikeRatePerSeason } = require('./ipl.js');
const { WonMatchAndToss } = require('./ipl.js');
const { mostPlayerOfMatchAwardsPerSeason } = require('./ipl.js');
const { highestTimePlayerDismissed } = require('./ipl.js');
csv()
    .fromFile(matches)
    .then((jsonObj) => {

        let matchesData = jsonObj;

        csv()
            .fromFile(deliveries)
            .then((jsonObj) => {
                let deliveriesData = jsonObj;
                callFunction(matchesData, deliveriesData);
            })
    })



function callFunction(matchesData, deliveriesData) {

    matchesWonPerTeam(matchesData);
    matchesPlayedPerYear(matchesData);
    bestEconomyInSuperOver(deliveriesData);
    extraRunsConcededIn2016(matchesData, deliveriesData);
    topEconomicalBowler2015(matchesData, deliveriesData);
    strikeRatePerSeason(matchesData, deliveriesData);
    WonMatchAndToss(matchesData);
    mostPlayerOfMatchAwardsPerSeason(matchesData);
    highestTimePlayerDismissed(deliveriesData);
}






























