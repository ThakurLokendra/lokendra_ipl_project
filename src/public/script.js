
function extraRunsPerConcededIn2016() {
    fetch("./output/extraRunsConcededIn2016.json")
        .then(response => response.json())
        .then((data) => {
            let keys = Object.keys(data);
            let values = Object.values(data);
            Highcharts.chart('container1', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'EXTRA RUNS CONCEDED IN 2016',
                    fontFamily: 'sans-serif',

                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [...keys],
                    crosshair: true,
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Runs'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: '',
                    data: [...values]



                }
                ]
            });
        })
}
extraRunsPerConcededIn2016();



function matchesPlayedPerYear() {
    fetch("./output/matchesPlayedPerYear.json")
        .then(response => response.json())
        .then((data) => {
            const entries = Object.entries(data);

            Highcharts.chart('container2', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'MATCHES PLAYED PER YEAR',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Matches'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Matches : <b>{point.y:.1f} </b>'
                },
                series: [{
                    name: 'Mactches',
                    data: entries,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.0f}',
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        })
}

matchesPlayedPerYear();



function topEconomicalBowler2015() {
    fetch("./output/topEconomicalBowler2015.json")
        .then(response => response.json())
        .then((data) => {
            let bowlerName = [];
            let economy = [];
            for (let i = 0; i < data.length; i++) {
                bowlerName.push(data[i][0]);
                economy.push(Number.parseFloat(data[i][1]));
            }

            const chart = Highcharts.chart('container3', {
                title: {
                    text: 'TOP ECONOMICAL RATED BOWLER IN 2015',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: bowlerName
                },
                series: [{
                    type: 'column',
                    colorByPoint: true,
                    data: economy,
                    showInLegend: false
                }]
            });

            document.getElementById('plain').addEventListener('click', () => {
                chart.update({
                    chart: {
                        inverted: false,
                        polar: false
                    },
                    subtitle: {
                        text: 'Plain'
                    }
                });
            });

            document.getElementById('inverted').addEventListener('click', () => {
                chart.update({
                    chart: {
                        inverted: true,
                        polar: false
                    },
                    subtitle: {
                        text: 'Inverted'
                    }
                });
            });

            document.getElementById('polar').addEventListener('click', () => {
                chart.update({
                    chart: {
                        inverted: false,
                        polar: true
                    },
                    subtitle: {
                        text: 'Polar'
                    }
                });
            });
        })

}

topEconomicalBowler2015();


function wonMatchAndToss() {
    fetch("./output/wonMatchAndToss.json")
        .then(response => response.json())
        .then((data) => {
            let arrayData = [];
            for (const key in data) {
                arrayData.push({ name: key, y: data[key] })
            }

            Highcharts.chart('container4', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'TEAM THAT WON MATCHES AND TOSS',
                    fontFamily: 'sans-serif'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.0f}</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: ''
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: '',
                    colorByPoint: true,
                    data: arrayData
                }]
            });
        })



}

wonMatchAndToss();


function matchesWonPerTeam() {
    fetch("./output/matchesWonPerTeam.json")
        .then(response => response.json())
        .then((data) => {
            let keys = Object.keys(data);
            let yearsArray = ["2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"];

            let dataArray = [];
            for (let index = 0; index < yearsArray.length; index++) {
                let indivisualYearData = [];
                for (const key in data) {
                    let flag = false;
                    let entries = Object.entries(data[key]);
                    for (let innerIndex = 0; innerIndex < entries.length; innerIndex++) {
                        if (entries[innerIndex][0] === yearsArray[index]) {
                            indivisualYearData.push(entries[innerIndex][1]);
                            flag = true;
                        }
                    }
                    if (flag == false) {
                        indivisualYearData.push(0);
                    }
                }
                dataArray.push({ name: yearsArray[index], data: indivisualYearData });
            }

            Highcharts.chart('container5', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'MATCHES WON PER TEAM PER SEASON',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: keys,
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ''
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 5,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: dataArray
            });
        })

}

matchesWonPerTeam();

function mostPlayerOfMatchAwardsPerSeason() {
    fetch("./output/mostPlayerOfMatchAwardsPerSeason.json")
        .then(response => response.json())
        .then((data) => {

            let dataArray = [];

            for (const key in data) {
                let entries = Object.entries(data[key]);
                let blankElement = Number.parseInt(key) - 2008;
                let value = 0;
                let mergeArray = [];
                while (value < blankElement) {
                    mergeArray.push('');
                    value++;
                }
                for (let index = 0; index < entries.length; index++) {
                    mergeArray.push(entries[index][1]);
                    dataArray.push({ name: entries[index][0], data: mergeArray })
                    mergeArray = mergeArray.slice(0, mergeArray.length - 1);
                }

            }

            Highcharts.chart('container6', {
                chart: {
                    type: 'spline'
                },

                legend: {
                    symbolWidth: 40.
                },

                title: {
                    text: 'MOST PLAYER OF MATCH PER SEASON',
                    fontFamily: 'sans-serif'
                },

                subtitle: {
                    text: ''
                },

                yAxis: {
                    title: {
                        text: 'Award Numbers'
                    },
                    accessibility: {
                        description: 'Award Numbers'
                    }
                },

                xAxis: {
                    title: {
                        text: 'Years'
                    },
                    accessibility: {
                        description: ''
                    },
                    categories: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
                },

                tooltip: {
                    valueSuffix: ''
                },

                plotOptions: {
                    series: {
                        point: {
                            events: {
                                click: function () {
                                    window.location.href = this.series.options.website;
                                }
                            }
                        },
                        cursor: 'pointer'
                    }
                },

                series: dataArray,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 550
                        },
                        chartOptions: {
                            chart: {
                                spacingLeft: 3,
                                spacingRight: 3
                            },
                            legend: {
                                itemWidth: 150
                            },
                            xAxis: {
                                categories: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017'],
                                title: ''
                            },
                            yAxis: {
                                visible: false
                            }
                        }
                    }]
                }
            });

        })


}

mostPlayerOfMatchAwardsPerSeason();


function bestEconomyBowlerInsuperOvers() {
    fetch("./output/bestEconomicalBowlerInSuperOver.json")
        .then(response => response.json())
        .then((data) => {
            let dataArray = [];
            let entries = Object.entries(data);
            dataArray.push(entries);
            Highcharts.chart('container7', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45
                    }
                },
                title: {
                    text: 'BEST ECONOMICAL BOWLER IN SUPER OVERS',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                plotOptions: {
                    pie: {
                        innerSize: 100,
                        depth: 45
                    }
                },
                series: [{
                    name: 'Economy',
                    data: [
                        ...entries
                    ]
                }]
            });


        })

}


bestEconomyBowlerInsuperOvers();

function highestTimePlayerDismissed() {
    fetch("./output/highestTimePlayerDismissed.json")
        .then(response => response.json())
        .then((data) => {
            const dataArray = [];
            const keys = Object.keys(data);
            for (const key in data) {
                let array = Object.entries(data[key]);
                for (let index = 0; index < array.length; index++) {
                    let arraytoStore = []
                    arraytoStore.push(array[index][1]);
                    dataArray.push({ name: array[index][0], data: arraytoStore })
                }

            }

            Highcharts.chart('container8', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'HIGHEST TIME PLAYER DISMISSED BY ANOTHER PLAYER',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [...keys],
                    title: {
                        text: 'player dismissed'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' '
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: dataArray
            });

        })
}


highestTimePlayerDismissed();

function strikeRateOfAPlayerPerSeason(){
    
    fetch("./output/strikeRateOfAPlayerPerSeason.json")
        .then(response => response.json())
        .then(data =>{
            let batsmanStrikeRate = [];
            for(let player in data){

                let strikeRates = [];
                for(let year in data[player]){
                    strikeRates.push(Number.parseFloat(data[player][year]));
                }
                batsmanStrikeRate.push({
                    name : player,
                    data : strikeRates
                })
            }
            Highcharts.chart('container9', {

                title: {
                    text: 'STRIKE RATE OF VRAT KOHLI FROM 2008 TO 2017',
                    fontFamily: 'sans-serif'
                },
            
                subtitle: {
                    text: ''
                },
            
                yAxis: {
                  
                    title: {
                        text: 'Strike Rate'
                    }
                },
            
                xAxis: {
                    accessibility: {
                        rangeDescription: 'Range: 2008 to 2017'
                    },
                    title:{
                        text: 'Years'
                    }                    
                },
            
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
            
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2008
                    }
                },
            
                series: batsmanStrikeRate,
            
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            
            });
        });
}

strikeRateOfAPlayerPerSeason();
